<?php

namespace Elements\BillysBilling\BillyClient;

/**
 * Class BillyClient
 *
 * @package Elements\BillysBilling\BillyClient
 */
class BillyClient {

	/**
	 * @var
	 */
	private $accessToken;

	/**
	 * @param $accessToken
	 */
	public function __construct($accessToken) {
		$this->accessToken = $accessToken;
	}

	/**
	 * @todo Use something else than raw curl to make the request
	 *
	 * @param $method
	 * @param $url
	 * @param null $body
	 * @return object
	 */
	public function request($method, $url, $body = null) {
		$headers = ["X-Access-Token: " . $this->accessToken];
		$c       = curl_init("https://api.billysbilling.com/v2" . $url);
		curl_setopt($c, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
		if ($body) {
			curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($body));
			$headers[] = "Content-Type: application/json";
		}
		curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
		$res  = curl_exec($c);
		$body = json_decode($res);
		$info = curl_getinfo($c);

		return (object) [
			'status' => $info['http_code'],
			'body'   => $body
		];
	}

}
